# 2017 BR review heatmaps

###project is laid out according to ProjectTemplate R package in the `br_review/` folder

`br_review/data/heatmap_lists`: contains the input lists to make the heatmaps
	
* `bes1_rd_target_ann.csv`: contains info for genes that are BES1/BZR1 targets, RD26 targets during ABA treatment (2016 Science from Ecker lab) or both BES1/BZR1 and RD26 targets. 

`br_review/src/RD26heatmap_review_2-2017.R`: code to generate the heatmaps


# Drought papers 

1. RD26 Nat communications

2. Farnesylation mediates brassinosteroidbiosynthesis to regulate abscisic acid responses

	* shows that CYP85A2, the enzyme that converts castasterone to BL needs to be farnesylated in order to funciton. also looks at BR drought relationship
	* farnesyl transferase ERA1 (ENHANCED RESPONSE TOABSCISIC ACID 1) is a neg. reg. of ABA response. functions as β-subunit of the heterodimeric farnesyl transferase enzyme. 
		* era1-2 mutant has reduced BR, smaller round leaves and is more sensitive to BRZ (also protruding carpels). The phenotypes could be rescued by BL treatment. 
	* phenotypes of era1-2 explained due to CYP85A2 needing to be farnesylated by it. showed that it is a target in vitro and in vivo and then if you mutate the farnesylation sites that cyp85a2-2 mutant can be rescued (some but not all phenos). 
	* farnsylation req'd for proper localization of CYP85A2 to ER subdomains. 

3. Down-regulation of BdBRI1, a putative brassinosteroid receptor gene produces a dwarf phenotype with enhanced drought tolerance in Brachypodium distachyon

4. Brassinosteroid confers tolerance in Arabidopsis thaliana and Brassica napus to a range of abiotic stresses. Planta 225, 353–364 (2007).

5. Effects of brassinosteroids on the plant responses to environmental stresses. Plant Physiol. Biochem. 47, 1–8 (2009).

6. Brassinolide application improves the drought tolerance in maize through modulation of enzymatic antioxidants and leaf gas exchange. J. Agron. Crop Sci. 197, 177–185 (2011).
7. Effect of brassinosteroids on drought resistance and abscisic acid concentration in tomato under water stress. Sci. Horticult. 126, 103–108 (2010).
8. H2O2 mediates the crosstalk of brassinosteroid and abscisic acid in tomato responses to heat and oxidative stresses. J. Exp. Bot. 65, 4371–4383 (2014).
	* "A detailed examination of this process in tomato revealed that exogenous 	brassinosteroid application results in ABA accumulation via H2O2 that was 	transiently induced by brassinosteroid"


	
# Papers connecting ABA and BR
1. **Ryu 2014 - BES1/TPL/HDAC19/ABI3** - BES1 attenuates ABI5 expression via ABI3 by binding inducing histone deacetylation of ABI3 promoter, inhibiting ABI3s activation of ABI5. 

2. **Cai 2014 - BIN2-SNRK2**; BIN2 phosphorylates SnRK2.3, which is important for its activation. 
	* Thr180 mutation to A reduced phos of SnRK by BIN2 and caused SnRK to lose its ability to phos. substrate. 

3. **Steber and McCourt 2001 Plant phys.** 

4. Also early BR paper (Clouse 1996?) mentioned in his review and to me at ASPB

5.  **BRASSINOSTEROID INSENSITIVE2 interacts with ABSCISIC ACID INSENSITIVE5 to mediate the antagonism of brassinosteroids to abscisic acid during seed germination in Arabidopsis. Plant Cell 26, 4394–4408 (2014).** 
	* BIN2 phosphorylates and stabilizes ABI5, which is important during seed germination.

6. **OsREM4.1 Interacts with OsSERK1 to Coordinate the
Interlinking between Abscisic Acid and
Brassinosteroid Signaling in Rice (Dev Cell 2016).**
	* shows that ABA inducible remorin protein in rice is induced by ABA via BZIP23 and ineracts with the activation look of OsBAK1 to inhibit BRI1-BAK1 transphosphorylation. When BR levles are increased, OsBRI1 can phosphorylate OsREM4.1 which aleviates the inhibition of REM4.1 on BAK1. 
	* Refered to by nice commentary by Steve Clouse : Brassinosteroid/Abscisic Acid Antagonism in Balancing Growth and Stress 

7. **The antagonistic regulation of abscisic acid-inhibited root growth by brassinosteroids is partially mediated via direct suppression of ABSCISIC ACID INSENSITIVE 5 expression by BRASSINAZOLE RESISTANT 1**(PCE 2016)
	* looked at effect of ABA response in BR mutant roots; did sequencing to find BZR1 and BIN2 reg. gene overlap with ABA in roots. ChIP and GMSAs show that BZR1 binds to G-box of ABI5 promoter to suppresses its expression. ABI5 OE in bzr1-D background restored ABA sensitivity (partially). 
	* bzr1-D partially rescues bin2-1 ABA phenos
	* RNA-seq overlap of bin2-1 with ABA is high, but bin2-1 with snRK low.
	* overlap of bin2-1 with ABA (39.2%) was also higher than BZR1 (17,94%)  
	* bin2-T more insensitive to ABA than bzr1-D (BIN2 more major role?). 
	* in contrast to RYU 2014, didn't find that ABI3 altered in bzr1-D, suggesting that BZR1 may directly reg. ABI5. 

8. **The Short-Rooted Phenotype of the brevis radix Mutant Partly Reflects Root Abscisic Acid Hypersensitivity1[C][W][OA]
Americo Rodrigues** - Rodrigues 2009 Plant Physiol. 

# Drought Figures
### 1. Model of crosstalk of BR and drought pathways
* sketch this out

### 2. Comparison of RD26 RNA-seq/ChIP data to BES1 targets
* venny overlaps
* some kind of clustering? Maybe with common genes in RDOX, rdq and with BRs?  
	* or use all drought genes and annotate RD targets, BES1 targets and RD and BES1 targets


